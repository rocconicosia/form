package lang


/*****
 * Recursive structure containing a local symbol table. 
 */
type scope struct {
  parent *scope
  defs map[sym]sexpr
}

/*****
 * Sets the value of a symbol within a specified scope. 
 */
func (sc *scope) set(symbol sym, value sexpr) {
  if sc.parent != nil && sc.parent.isDefined(symbol) {
    sc.parent.set(symbol, value)
  } else {
    sc.defs[symbol] = value
  }
}

/*****
 * Returns the value of a symbol located within a specified scope. 
 * Panics if symbol cannot be found.
 */
func (sc *scope) lookup(symbol sym) sexpr {
  if _, ok := sc.defs[symbol]; ok {
    return sc.defs[symbol]
  }
  if sc.parent == nil {
    panic("scope.go: lookup() error, does not exist")
  }
  return sc.parent.lookup(symbol)
}

/*****
 * Determines the existence of a symbol within a specified scope.
 */
func (sc *scope) isDefined(symbol sym) bool {
  if _, ok := sc.defs[symbol]; ok {
    return true
  }
  if sc.parent == nil {
    return false
  }
  return sc.parent.isDefined(symbol)
}
