package lang


// S-expressions - all values in the language
type sexpr interface{}

// Symbols - immutable strings such as identifiers
type sym string

// Functions - all builtins must implement this prototype 
type function func(*scope, []sexpr) sexpr

// Construct - single cell of a recursive list structure 
type cons struct {
  car sexpr
  cdr sexpr
}

func isFunction(exp sexpr) bool {
  _, ok := exp.(function)
  return ok
}
