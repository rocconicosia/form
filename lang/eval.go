package lang


/*****
 * Reduces an entire s-expression into a single value.
 */
func eval(sc *scope, exp sexpr) sexpr {
  switch e := exp.(type) {
    case cons:
      car := eval(sc, e.car)
      if isFunction(car) {
        fn := car.(function)
        return apply(sc, fn, flatten(e.cdr))
      }
      return e
    case sym:
      return sc.lookup(e)
  }
  return exp
}

/*****
 * Applies a function to a flattened s-expression.
 */
func apply(sc *scope, fn function, args []sexpr) sexpr {
  for i, expr := range args {
    args[i] = eval(sc, expr)
  }
  return fn(sc, args)
}

/*****
 * Expand the current s-expression into a list. 
 */
func flatten(exp sexpr) (exps []sexpr) {
  c, ok := exp.(cons)
  for ok {
    exps = append(exps, c.car)
    c, ok = c.cdr.(cons)
  }
  return
}

/*****
 * Reduce a list of terms into an s-expression. 
 */
func unflatten(exps []sexpr) sexpr {
  c := sexpr(nil)
  for i := len(exps) - 1; i >= 0; i-- {
    c = cons{exps[i], c}
  }
  return c
}
