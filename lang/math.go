package lang 

/*****
 * Addition function. For testing purposes. 
 */
func func_add(sc *scope, args []sexpr) sexpr {
  r := 0.0
  for _, arg := range args {
    n, ok := arg.(float64)
    if !ok {
      panic("Invalid arguments to add")
    }
    r += n
  }
  return r
}