package lang

import(
  "io/ioutil"
  "os"
  "strconv"
  "strings"
)

type lexer struct {
  idx int
  source []byte
  spaceSeen bool
}

func (lx *lexer) newSource(filename string) {
  file, _ := os.Open(filename)
  lx.source, _ = ioutil.ReadFile(filename)
  file.Close()
}

func (lx *lexer) read() byte {
  c := lx.source[lx.idx]
  lx.idx++
  return c
}

func (lx *lexer) unread() { lx.idx-- }

func (lx *lexer) next() sexpr {
  ch := string(lx.read())

  if ch == "(" {
      return lx.left_paren(ch)
  } else if ch == ")" {
      return lx.right_paren(ch)
  } else if _, err := strconv.Atoi(ch); err == nil {
      return lx.number(ch)
  } else if ch == "+" {
      return lx.plus(ch)
  } else if ch == " " {
      lx.spaceSeen = true
  }
  return nil
}

func (lx *lexer) hasNext() bool {
  return lx.idx != len(lx.source) - 1
}

func (lx *lexer) left_paren(ch string) sexpr {
  lx.spaceSeen = false
  return ch
}

func (lx *lexer) right_paren(ch string) sexpr {
  lx.spaceSeen = false
  return ch
}

func (lx *lexer) number(ch string) sexpr {
  tok := []string{}

  _, err := strconv.Atoi(ch)
  for err == nil{
    tok = append(tok, ch)
    ch = string(lx.read())
    _, err = strconv.Atoi(ch)
  }

  lx.unread()
  num, _ := strconv.Atoi(strings.Join(tok, ""))

  lx.spaceSeen = false
  return float64(num)
}

func (lx *lexer) plus(ch string) sexpr {
  lx.spaceSeen = false
  return ch
}
